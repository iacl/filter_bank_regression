# arguments
INP_FPATH=$1
INP_FNAME=$(basename ${INP_FPATH})
INP_ID=${INP_FNAME%.nii*}

OUT_FPATH=$2
OUT_DIR="$(dirname $(readlink -f ${OUT_FPATH}))/${INP_ID}"

FWHM=$3
GAP=$4
GPU_ID=$5
ACQ_DIM=2
FILTERS_DIR="${OUT_DIR}/filters"

# Run stage 1
filter-bank-sr-stage-1\
    --inp-fpath ${INP_FPATH} \
    --weight-dir ${FILTERS_DIR} \
    --acq-dim ${ACQ_DIM} \
    --fwhm ${FWHM} \
    --gap ${GAP} \
    --n-steps 6400000\
    --gpu-id ${GPU_ID}
    

# Run stage 2
filter-bank-sr-stage-2\
    --inp-fpath ${INP_FPATH} \
    --out-fpath ${OUT_FPATH} \
    --filter-dir ${FILTERS_DIR} \
    --acq-dim ${ACQ_DIM} \
    --fwhm ${FWHM} \
    --gap ${GAP} \
    --n-steps 15000\
    --gpu-id ${GPU_ID}
