import os
from pathlib import Path
import sys

fwhm = int(sys.argv[1])
gap = int(sys.argv[2])
gpu_id = int(sys.argv[3])

N = 10
M = 20
data_dir = Path(
    f"/ISFILE3/USERS/remediossw/data/OASIS3/OASIS3_{fwhm:02d}x_{gap:02d}gap_aniso")
fpaths = sorted(data_dir.iterdir())[N:N+M]

for fpath in fpaths:
    subj = fpath.name.split('.')[0]
    out_dir = Path(
        f"/ISFILE3/USERS/remediossw/data/results/slice_gap/{subj}/fbreg/{fwhm}x_{gap}gap")
    if not out_dir.exists():
        out_dir.mkdir(parents=True)
    cmd = f"./scripts/run_full.sh {fpath} {out_dir} {fwhm} {gap} {gpu_id}"
    os.system(cmd)
