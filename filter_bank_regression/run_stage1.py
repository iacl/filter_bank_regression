from datetime import datetime
import numpy as np
from pathlib import Path
import nibabel as nib
import sys
import argparse
from tqdm import tqdm
import torch
import torch.nn as nn
from torch.utils.data import DataLoader

from degrade.degrade import select_kernel

from .utils.train_loader_1D_FB import TrainSet as TrainSet1D
from .utils.train_loader_1D_FB import normalize
from .utils.filter_bank_utils import *


class Network(nn.Module):
    def __init__(self, init_codes, features, device):
        super().__init__()

        self.L = len(init_codes[0])
        M = len(init_codes)

        # initial latent codes
        init_codes = [
            torch.from_numpy(init_code).to(device) for init_code in init_codes
        ]

        self.latent_codes = [
            torch.zeros_like(init_code, requires_grad=True, device=device)
            for init_code in init_codes
        ]
        for i in range(M):
            self.latent_codes[i].data = init_codes[i].data

    def forward(self):
        filters = [x for x in self.latent_codes]

        return filters


def modulate(h0, k, M, is_analysis=True, device="cpu"):
    n = torch.linspace(0, len(h0) - 1, len(h0), device=device)
    C = np.sqrt(2 / M)
    mod = C * torch.cos((k + 1 / 2) * (n + (M + 1) / 2) * (torch.pi / M))
    return h0 * mod


def main(args=None):
    #################### ARGUMENTS ####################
    parser = argparse.ArgumentParser()
    parser.add_argument("--inp-fpath", type=str, required=True)
    parser.add_argument("--weight-dir", type=str, required=True)
    parser.add_argument("--acq-dim", type=int, choices=[2, 3], default=2)
    parser.add_argument("--slice-profile-fpath", type=str, required=False)
    parser.add_argument("--fwhm", type=int, required=True)
    parser.add_argument("--gap", type=int, required=True)
    parser.add_argument("--gpu-id", type=int, required=True, default=0)
    parser.add_argument("--batch-size", type=int, default=64)
    parser.add_argument("--n-steps", type=int, default=50000)

    args = parser.parse_args(args if args is not None else sys.argv[1:])

    print("=" * 20, "STAGE 1", "=" * 20)

    inp_fpath = Path(args.inp_fpath)
    weight_dir = Path(args.weight_dir)
    if not weight_dir.exists():
        weight_dir.mkdir(parents=True)
    fwhm = args.fwhm
    gap = args.gap
    M = fwhm + gap

    device = device = torch.device(f"cuda:{args.gpu_id}")

    # Load up data
    batch_size = args.batch_size

    ds_stage_1 = TrainSet1D(
        vol_fpath=inp_fpath,
        lr_dim=args.acq_dim,
        M=M,
        n_items=args.n_steps,
    )
    loader_stage_1 = DataLoader(
        ds_stage_1,
        batch_size=batch_size,
        shuffle=True,
        pin_memory=True,
        num_workers=8,
    )

    # Load h0
    if args.slice_profile_fpath:
        h0 = np.load(Path(args.slice_profile_fpath))
    else:
        h0 = select_kernel(21, window_choice="gaussian", fwhm=fwhm).astype(np.float32)
    h0 = torch.from_numpy(h0).to(device)

    # Initialize filters
    hs = [h0]
    for k in range(1, M):
        h_k = modulate(h0, k, M, device=device)
        hs.append(h_k)
    hs = [h.detach().cpu().numpy() for h in hs]

    # discard first filter; we're given h0 and don't need to learn it
    # Similarly, copy h0 as f0
    init_codes = hs[1:] * 2
    init_codes = [init_code.astype(np.float32) for init_code in init_codes]

    G = Network(
        init_codes=init_codes,
        features=len(init_codes[0]) * 256,
        device=device,
    ).to(device)

    # Learning params
    lr = 1e-1
    opt = torch.optim.AdamW(G.latent_codes, lr=lr)

    loss_fn = torch.nn.MSELoss().to(device)

    scheduler = torch.optim.lr_scheduler.OneCycleLR(
        optimizer=opt,
        max_lr=lr,
        total_steps=args.n_steps + 1,
        cycle_momentum=True,
    )

    opt.step()

    with tqdm(total=args.n_steps) as pbar:
        pbar_dict = {
            "loss": np.finfo(np.float32).max,
        }
        for cur_batch, xs_cpu in enumerate(loader_stage_1):
            opt.zero_grad()

            xs = xs_cpu.to(device)

            filters = G()
            hs = [h0, *filters[: M - 1]]
            fs = [h0, *filters[M - 1 :]]

            coefs = analyze(xs, hs)
            xs_hat = synthesize(coefs, fs)

            loss = loss_fn(xs_hat, xs)
            loss.backward()
            opt.step()

            if cur_batch % 5 == 0:
                pbar_dict["loss"] = loss.detach().cpu().numpy().item()
                pbar.set_postfix(
                    {k: f"{v:.6f}" for k, v in pbar_dict.items() if v != 0}
                )

            scheduler.step()
            pbar.update(batch_size)

    # Save results
    torch.save([h.detach().cpu() for h in hs], weight_dir / "hs.pt")
    torch.save([f.detach().cpu() for f in fs], weight_dir / "fs.pt")
