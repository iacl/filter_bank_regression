'''
A convolutional autoencoder using ConvNeXt blocks.

ConvNeXt blocks adapted from: https://github.com/facebookresearch/ConvNeXt/blob/main/models/convnext.py
'''

import torch
import torch.nn as nn
import torch.nn.functional as F

class LayerNorm(nn.Module):
    r""" LayerNorm that supports two data formats: channels_last (default) or channels_first. 
    The ordering of the dimensions in the inputs. channels_last corresponds to inputs with 
    shape (batch_size, height, width, channels) while channels_first corresponds to inputs 
    with shape (batch_size, channels, height, width).
    """
    def __init__(self, normalized_shape, eps=1e-6, data_format="channels_last"):
        super().__init__()
        self.weight = nn.Parameter(torch.ones(normalized_shape))
        self.bias = nn.Parameter(torch.zeros(normalized_shape))
        self.eps = eps
        self.data_format = data_format
        if self.data_format not in ["channels_last", "channels_first"]:
            raise NotImplementedError 
        self.normalized_shape = (normalized_shape, )
    
    def forward(self, x):
        if self.data_format == "channels_last":
            return F.layer_norm(x, self.normalized_shape, self.weight, self.bias, self.eps)
        elif self.data_format == "channels_first":
            u = x.mean(1, keepdim=True)
            s = (x - u).pow(2).mean(1, keepdim=True)
            x = (x - u) / torch.sqrt(s + self.eps)
            x = self.weight[:, None, None] * x + self.bias[:, None, None]
            return x

class Block(nn.Module):
    r""" ConvNeXt Block. There are two equivalent implementations:
    (1) DwConv -> LayerNorm (channels_first) -> 1x1 Conv -> GELU -> 1x1 Conv; all in (N, C, H, W)
    (2) DwConv -> Permute to (N, H, W, C); LayerNorm (channels_last) -> Linear -> GELU -> Linear; Permute back
    We use (2) as we find it slightly faster in PyTorch
    
    Args:
        dim (int): Number of input channels.
        drop_path (float): Stochastic depth rate. Default: 0.0
        layer_scale_init_value (float): Init value for Layer Scale. Default: 1e-6.
    """
    def __init__(self, dim, layer_scale_init_value=1e-6):
        super().__init__()
        self.dwconv = nn.Conv2d(dim, dim, kernel_size=7, padding=3, groups=dim) # depthwise conv
        self.norm = LayerNorm(dim, eps=1e-6)
        self.pwconv1 = nn.Linear(dim, 4 * dim) # pointwise/1x1 convs, implemented with linear layers
        self.act = nn.GELU()
        self.pwconv2 = nn.Linear(4 * dim, dim)
        self.gamma = nn.Parameter(layer_scale_init_value * torch.ones((dim)), 
                                    requires_grad=True) if layer_scale_init_value > 0 else None
        
    def forward(self, x):
        inp = x
        x = self.dwconv(x)
        x = x.permute(0, 2, 3, 1) # (N, C, H, W) -> (N, H, W, C)
        x = self.norm(x)
        x = self.pwconv1(x)
        x = self.act(x)
        x = self.pwconv2(x)
        if self.gamma is not None:
            x = self.gamma * x
        x = x.permute(0, 3, 1, 2) # (N, H, W, C) -> (N, C, H, W)

        x = inp + x
        return x
    
class ConvNeXtAE(nn.Module):
    '''
    Adapted from https://github.com/facebookresearch/ConvNeXt/blob/main/models/convnext.py
    into an AE format.
    '''
    
    r""" ConvNeXt
        A PyTorch impl of : `A ConvNet for the 2020s`  -
          https://arxiv.org/pdf/2201.03545.pdf
    Args:
        in_chans (int): Number of input image channels. Default: 3
        depths (tuple(int)): Number of blocks at each stage. Default: [3, 3, 9, 3]
        dims (int): Feature dimension at each stage. Default: [96, 192, 384, 768]
        layer_scale_init_value (float): Init value for Layer Scale. Default: 1e-6.
    """
    def __init__(
        self, 
        in_chans=3,
        depths=[3, 3, 9, 3], 
        dims=[96, 192, 384, 768], 
        layer_scale_init_value=1e-6,
    ):
        super().__init__()
                
        # encode the image into a convolutional latent space
        self.encoder = nn.Sequential(
            # Stage 0 downsampling; initial conv from image space
            nn.Conv2d(in_chans, dims[0], kernel_size=4, stride=4),
            LayerNorm(dims[0], eps=1e-6, data_format="channels_first"),
            # Stage 0 blocks repeated `depths[0]` times
            *[Block(dim=dims[0], layer_scale_init_value=layer_scale_init_value) for _ in range(depths[0])],
            # Stage 1 downsampling
            LayerNorm(dims[0], eps=1e-6, data_format="channels_first"),
            nn.Conv2d(dims[0], dims[1], kernel_size=2, stride=2),
            # Stage 1 blocks repeated `depths[1]` times
            *[Block(dim=dims[1], layer_scale_init_value=layer_scale_init_value) for _ in range(depths[1])],
            # Stage 2 downsampling
            LayerNorm(dims[1], eps=1e-6, data_format="channels_first"),
            nn.Conv2d(dims[1], dims[2], kernel_size=2, stride=2),
            # Stage 2 blocks repeated `depths[2]` times
            *[Block(dim=dims[2], layer_scale_init_value=layer_scale_init_value) for _ in range(depths[2])],
            # Stage 3 downsampling
            LayerNorm(dims[2], eps=1e-6, data_format="channels_first"),
            nn.Conv2d(dims[2], dims[3], kernel_size=2, stride=2),
            # Stage 3 blocks repeated `depths[3]` times
            *[Block(dim=dims[3], layer_scale_init_value=layer_scale_init_value) for _ in range(depths[3])],
        )
        
        self.decoder = nn.Sequential(
            # Stage 3 blocks repeated `depths[3]` times
            *[Block(dim=dims[3], layer_scale_init_value=layer_scale_init_value) for _ in range(depths[3])],
            # Stage 3 upsampling
            LayerNorm(dims[3], eps=1e-6, data_format="channels_first"),
            nn.ConvTranspose2d(dims[3], dims[2], kernel_size=2, stride=2),
            # Stage 2 blocks repeated `depths[2]` times
            *[Block(dim=dims[2], layer_scale_init_value=layer_scale_init_value) for _ in range(depths[2])],
            # Stage 2 upsampling
            LayerNorm(dims[2], eps=1e-6, data_format="channels_first"),
            nn.ConvTranspose2d(dims[2], dims[1], kernel_size=2, stride=2),
            # Stage 1 blocks repeated `depths[1]` times
            *[Block(dim=dims[1], layer_scale_init_value=layer_scale_init_value) for _ in range(depths[1])],
            # Stage 1 upsampling
            LayerNorm(dims[1], eps=1e-6, data_format="channels_first"),
            nn.ConvTranspose2d(dims[1], dims[0], kernel_size=2, stride=2),
            # Stage 0 blocks repeated `depths[0]` times
            *[Block(dim=dims[0], layer_scale_init_value=layer_scale_init_value) for _ in range(depths[0])],
            # Stage 0 upsampling to image space
            LayerNorm(dims[0], eps=1e-6, data_format="channels_first"),
            nn.ConvTranspose2d(dims[0], dims[0], kernel_size=4, stride=4),
            # Final convolution to clean up this upsampling
            nn.Conv2d(dims[0], in_chans, kernel_size=3, stride=1, padding='same'),
            # Final activation
            nn.Tanh(),
        )

    def forward(self, x):
        x = self.encoder(x)
        x = self.decoder(x)
        return x
