import torch
from torch import nn
import numpy as np


class ResBlock(nn.Module):
    def __init__(
            self,
            in_channels,
            out_channels,
            kernel_size,
            res_scale,
            padding_mode,
    ):
        super(ResBlock, self).__init__()

        padding = kernel_size // 2

        self.body = nn.Sequential(
            nn.Conv2d(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            nn.ReLU(inplace=True),
            nn.Conv2d(
                in_channels=out_channels,
                out_channels=out_channels,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
        )

        self.res_scale = res_scale

    def forward(self, x):
        out = self.body(x).mul(self.res_scale)
        return out + x


class EDSR(nn.Module):
    def __init__(
            self,
            n_channels,
            patch_size=32,
            kernel_size=3,
            n_resblocks=32,
            filters=256,
            padding_mode='zeros',
            min_clip=0,
    ):
        super(EDSR, self).__init__()
        padding = kernel_size // 2
        self.min_clip = min_clip
        self.pred_fov = patch_size

        # initial convolution
        self.head = nn.Sequential(
            nn.Conv2d(
                in_channels=n_channels,
                out_channels=filters,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            nn.ReLU(inplace=True),
        )

        self.body = nn.Sequential(
            *[
                ResBlock(
                    in_channels=filters,
                    out_channels=filters,
                    kernel_size=kernel_size,
                    res_scale=0.1,
                    padding_mode=padding_mode,
                ) for _ in range(n_resblocks)
            ],
            nn.Conv2d(
                in_channels=filters,
                out_channels=filters,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            nn.ReLU(inplace=True),
        )

        # tail convolution
        self.tail = nn.Sequential(
            nn.Conv2d(
                in_channels=filters,
                out_channels=n_channels,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
        )

    def forward(self, x):
        x = self.head(x)
        res = self.body(x)
        out = self.tail(res + x)
        out = torch.clip(out, min=self.min_clip)
        return out

    def forward_in_chunks(self, x, chunk_size, target_shape, device):
        out = np.zeros(target_shape, dtype=x.dtype)
        for i in range(0, x.shape[0], self.pred_fov):
            for j in range(0, x.shape[1], self.pred_fov):
                chunk = x[i:i+chunk_size, j:j+chunk_size]

                pads = [(np.abs(a - b) // 2, np.abs(a - b) // 2)
                        for a, b in zip(chunk.shape, (chunk_size, chunk_size))]
                crops = [slice(p[0], -p[1]) for p in pads]
                crops = [c if c != slice(0, 0) else slice(
                    None, None) for c in crops]
                crops = tuple(crops)
                # Pad chunk if necessary
                chunk = np.pad(chunk, pads, mode='reflect')
                # Run network
                chunk_pred = self.forward(
                    torch.from_numpy(chunk).to(device).permute(
                        1, 0).unsqueeze(0).unsqueeze(1)
                ).detach().cpu().squeeze().permute(1, 0).numpy()

                fov_crop = [(np.abs(a - b) // 2, np.abs(a - b) // 2)
                            for a, b in zip(chunk_pred.shape, (self.pred_fov, self.pred_fov))]
                fov_crop = [slice(p[0], -p[1]) for p in fov_crop]
                fov_crop = [c if c != slice(0, 0) else slice(
                    None, None) for c in fov_crop]
                fov_crop = tuple(fov_crop)
                chunk_pred = chunk_pred[fov_crop]

                # Crop edge padding if necessary
                chunk_pred = chunk_pred[crops]

                # Crop more at boundaries if necessary
                crop = [(np.abs(a - b) // 2, np.abs(a - b) // 2)
                        for a, b in zip(out[i:i+self.pred_fov, j:j+self.pred_fov].shape, chunk_pred.shape)]
                crop = [slice(p[0], -p[1]) for p in crop]
                crop = [c if c != slice(0, 0) else slice(
                    None, None) for c in crop]
                crop = tuple(crop)

                chunk_pred = chunk_pred[crop]

                # Concatenate results to the right spot
                out[i:i+self.pred_fov, j:j+self.pred_fov] = chunk_pred
        return out
