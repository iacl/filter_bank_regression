import numpy as np
from torch import nn
import torch


class ResBlock(nn.Module):
    def __init__(
            self,
            in_channels,
            out_channels,
            kernel_size,
            res_scale,
            padding_mode,
    ):
        super(ResBlock, self).__init__()

        padding = kernel_size // 2

        self.body = nn.Sequential(
            nn.Conv2d(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            nn.Tanh(),
            nn.Conv2d(
                in_channels=out_channels,
                out_channels=out_channels,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
        )

        self.res_scale = res_scale

    def forward(self, x):
        out = self.body(x).mul(self.res_scale)
        return out + x


class UpsamplingBlock(nn.Module):
    def __init__(
            self,
            in_channels,
            out_channels,
            kernel_size,
            padding_mode,
            n_resblocks,
    ):
        super(UpsamplingBlock, self).__init__()

        padding = kernel_size // 2
        scale_factor = 2

        self.head = nn.Sequential(
            nn.Conv2d(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            nn.Tanh(),
            nn.Upsample(scale_factor=scale_factor, mode='bicubic', align_corners=False),
            nn.Conv2d(
                in_channels=out_channels,
                out_channels=out_channels,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            nn.Tanh(),
        )
        
        # Residual body following the UpSampling
        self.body = nn.Sequential(
            *[
                ResBlock(
                    in_channels=out_channels,
                    out_channels=out_channels,
                    kernel_size=kernel_size,
                    res_scale=0.1,
                    padding_mode=padding_mode,
                ) for _ in range(n_resblocks)
            ],
            nn.Conv2d(
                in_channels=out_channels,
                out_channels=out_channels,
                kernel_size=kernel_size,
                stride=1,
                padding=padding,
                padding_mode=padding_mode,
                bias=True,
            ),
            nn.Tanh(),
        )

    def forward(self, x):
        return self.body(self.head(x))


class Generator(nn.Module):
    '''
    Simple generator based on the SMORE work,
    mapping an N x M x C image and an NxMx1 noise
    to a kN x kM x C image, where k is a power of 2.
    '''
    def __init__(
        self,
        n_channels,
        img_resolution,
        init_size,
        z_dim,
        batch_size,
        kernel_size=3,
        n_resblocks=32,
        filters=256,
        padding_mode='zeros',
    ):
        super().__init__()
        padding = kernel_size // 2
        
        self.filters = filters
        self.batch_size = batch_size
        self.img_resolution = img_resolution
        self.init_size = init_size
        self.z_dim = z_dim
        self.num_layers = int(np.log2(self.img_resolution / self.init_size))
        
        # Map noise into a shape
        self.head = nn.Sequential(
            nn.Linear(self.z_dim, filters * self.init_size ** 2),
            nn.LeakyReLU(),
        )

        self.up_block = nn.Sequential(*[
            UpsamplingBlock(
                in_channels=filters,
                out_channels=filters,
                kernel_size=kernel_size,
                padding_mode=padding_mode,
                n_resblocks=n_resblocks,
            ) for _ in range(self.num_layers)
        ])

        self.tail = nn.Conv2d(
            in_channels=filters,
            out_channels=1,
            kernel_size=kernel_size,
            stride=1,
            padding=padding,
            padding_mode=padding_mode,
            bias=True,
        )

    def forward(self, z):
        # initial layers for shallow feature extraction
        x = self.head(z)     
        x = x.view(self.batch_size, self.filters, self.init_size, self.init_size) 
        x = self.up_block(x)
        x = self.tail(x)
        
        return x

class Discriminator(nn.Module):
    '''
    Very simple discriminator mapping a
    3-channel N x M input to a 3-channel N x M
    input in [0, 1].
    
    To account for detail coefficients at different scales,
    this model is actually a multi-model with K inputs
    and K outputs, where K is the number of levels
    in the wavelet decomposition.
    '''
    def __init__(
            self,
            n_levels,
            kernel_size=3,
            n_resblocks=32,
            filters=256,
            padding_mode='zeros',
    ):
        super(Discriminator, self).__init__()
        padding = kernel_size // 2
        self.n_levels = n_levels

        # initial convolution
        self.heads = nn.ModuleList([
            nn.Sequential(
                nn.Conv2d(
                    in_channels=3,
                    out_channels=filters,
                    kernel_size=kernel_size,
                    stride=1,
                    padding=padding,
                    padding_mode=padding_mode,
                    bias=True,
                ),
                nn.Tanh(),
            ) for level in range(self.n_levels)
        ])

        self.bodies = nn.ModuleList([
            nn.Sequential(
                *[
                    ResBlock(
                        in_channels=filters,
                        out_channels=filters,
                        kernel_size=kernel_size,
                        res_scale=0.1,
                        padding_mode=padding_mode,
                    ) for _ in range(n_resblocks)
                ],
                nn.Conv2d(
                    in_channels=filters,
                    out_channels=filters,
                    kernel_size=kernel_size,
                    stride=1,
                    padding=padding,
                    padding_mode=padding_mode,
                    bias=True,
                ),
                nn.Tanh(),
            ) for level in range(self.n_levels)
        ])

        # tail convolution
        # Every level produces 3 outputs
        self.tails = nn.ModuleList([
            nn.Sequential(
                nn.Conv2d(
                    in_channels=filters,
                    out_channels=3,
                    kernel_size=kernel_size,
                    stride=1,
                    padding=padding,
                    padding_mode=padding_mode,
                    bias=True,
                ),
                nn.Sigmoid(),
            ) for level in range(self.n_levels)
        ])

    def forward(self, xs):
        xs = [head(x) for x, head in zip(xs, self.heads)]
        ress = [body(x) for x, body in zip(xs, self.bodies)]
        outs = [tail(res) for res, tail in zip(ress, self.tails)]
        
        return outs
    
    
