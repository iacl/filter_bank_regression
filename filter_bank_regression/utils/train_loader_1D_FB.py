import numpy as np
from torch.utils.data import Dataset
import torch
import nibabel as nib
import time

from .patch_ops import *
from .augmentations import *


def normalize(x, a=-1, b=1):
    orig_min = x.min()
    orig_max = x.max()

    numer = (x - orig_min) * (b - a)
    denom = orig_max - orig_min

    return a + numer / denom, orig_min.item(), orig_max.item()


def inv_normalize(x, orig_min, orig_max, a, b):
    tmp = x - a
    tmp = tmp * (orig_max - orig_min)
    tmp = tmp / (b - a)
    tmp += orig_min
    return tmp


class TrainSet(Dataset):
    def __init__(
        self,
        vol_fpath,
        lr_dim,
        M,
        n_items,
        a=-1,
        b=1,
        dtype=np.float32,
    ):
        self.lr_dim = lr_dim
        self.n_items = n_items

        ##### Load the image volume #####
        self.vol = nib.load(vol_fpath).get_fdata(dtype=dtype)
        # move the LR dimension to the last spot
        self.vol = np.moveaxis(self.vol, self.lr_dim, -1)
        # pad to square in-plane
        p = np.abs(self.vol.shape[0] - self.vol.shape[1]) // 2
        idx_to_pad = np.argmin(self.vol.shape[:2])
        pads = [(0, 0), (0, 0)]
        pads.insert(idx_to_pad, (p, p))
        pads = tuple(pads)
        self.vol = np.pad(self.vol, pads)

        # now pad to nearest multiple of M
        pads = [(0, int(np.abs(d - np.ceil(d / M) * M))) for d in self.vol.shape]
        pads = tuple(pads)
        self.vol = np.pad(self.vol, pads)

        # normalize entire volume's intensity
        self.vol, self.orig_min, self.orig_max = normalize(self.vol, a=a, b=b)
        self.vol = torch.from_numpy(self.vol)

        # get batches of HR
        xs = torch.cat(
            [
                self.vol.flatten(start_dim=1, end_dim=2),
                self.vol.permute(1, 0, 2).flatten(start_dim=1, end_dim=2),
            ],
            dim=1,
        ).permute(1, 0)

        # prune non-zero intensities
        xs = xs[xs.sum(dim=1) != 0]

        self.xs = xs

    def __len__(self):
        return self.n_items

    def __getitem__(self, i):
        return self.xs[i % len(self.xs)]
