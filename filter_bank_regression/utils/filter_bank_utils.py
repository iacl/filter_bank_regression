import numpy as np
import torch
import torch.nn.functional as F
from resize.pytorch import resize
from scipy.interpolate import interp1d

from scipy import ndimage
from scipy.signal import windows

from degrade.degrade import select_kernel


def simulate_lr(vol, kernel, M, lr_axis=2):
    img_slice_batch = torch.moveaxis(vol, lr_axis, 0)
    D = img_slice_batch.shape[1]
    img_slice_1d = batch_2d_to_1d(img_slice_batch)
    lr_slice_1d = downsample_m(apply_filter(img_slice_1d, kernel), M)
    lr_slice_2d = batch_1d_to_2d(lr_slice_1d, D)
    return torch.moveaxis(lr_slice_2d, 0, lr_axis)


def synthesize(coefs, fs, filterless=False):
    """
    Execute the synthesis bank given the corresponding
    coefficients and filters.
    """
    M = len(fs)
    zs = list(range(M))

    if filterless:
        return torch.stack(
            [advance(upsample_m(coef, M), z) for coef, f, z in zip(coefs, fs, zs)],
            dim=0,
        ).sum(dim=0)

    synthesis = [
        advance(apply_filter(upsample_m(coef, M), f), z)
        for coef, f, z in zip(coefs, fs, zs)
    ]

    return torch.stack(synthesis, dim=0).sum(dim=0)


def analyze(signal, hs, filterless=False):
    M = len(hs)
    zs = list(range(M))

    if filterless:
        return [downsample_m(delay(signal, z), M) for h, z in zip(hs, zs)]

    analysis = [
        downsample_m(apply_filter(delay(signal, z), h), M) for h, z in zip(hs, zs)
    ]
    return analysis


def apply_filter(signal, filter_):
    return F.conv1d(
        signal.unsqueeze(1),
        filter_.unsqueeze(0).unsqueeze(1),
        stride=1,
        padding="same",
    ).squeeze(1)


def upsample_m(signal, M):
    """signal is a batch of 1D tensors; M is an integer"""
    B = signal.shape[0]
    L = signal.shape[1]

    up = torch.zeros((B, int(M * L)), dtype=signal.dtype, device=signal.device)
    up[:, ::M] = signal

    #     up = resize(signal.unsqueeze(0).unsqueeze(1), (1, 1/M), order=3).squeeze(1).squeeze(0)

    return up


def downsample_m(signal, M):
    return signal[:, ::M]


#     return resize(signal.unsqueeze(0).unsqueeze(1), (1, M), order=3).squeeze(1).squeeze(0)


def batch_to_vol(signal, D):
    """signal is a batch of 1D tensors; BxL"""
    L = signal.shape[1]
    return signal.reshape(D, D, L).numpy()


def vol_to_batch(signal):
    return torch.from_numpy(signal).flatten(start_dim=0, end_dim=1)


def batch_2d_to_1d(signal):
    return signal.flatten(start_dim=0, end_dim=1)


def batch_1d_to_2d(signal, D):
    L = signal.shape[1]
    B = signal.shape[0] // D
    return signal.reshape(B, D, L)


def advance(signal, amt):
    """signal is a batch of 1D Torch tensors; BxL"""
    if amt == 0:
        return signal
    return F.pad(signal[:, amt:], (0, amt), mode="constant", value=0.0)


def delay(signal, amt):
    """signal is a batch of 1D Torch tensors; BxL"""
    if amt == 0:
        return signal
    return F.pad(signal[:, :-amt], (amt, 0), mode="constant", value=0.0)


def calc_fwhm(kernel):
    """Calculates the full width at half maximum (FWHM) using linear interp.

    Args:
        kernel (numpy.ndarray): The kernel whose FWHM to be calculated.

    Returns
    -------
    fwhm: float
        The calculated FWHM. It is equal to ``right - left``.
    left: float
        The position of the left of the FWHM.
    right: float
        The position of the right of the FWHM.

    """
    kernel = kernel.squeeze()
    half_max = float(np.max(kernel)) / 2
    indices = np.where(kernel > half_max)[0]
    if len(indices) == 0:
        return 0, 0, 0

    left = indices[0]
    if left > 0:
        interp = interp1d((kernel[left - 1], kernel[left]), (left - 1, left))
        left = interp(half_max)
    right = indices[-1]
    if right < len(kernel) - 1:
        interp = interp1d((kernel[right + 1], kernel[right]), (right + 1, right))
        right = interp(half_max)
    fwhm = right - left
    return fwhm, left, right
