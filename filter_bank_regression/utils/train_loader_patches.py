import numpy as np
from torch.utils.data import Dataset
import torch
import nibabel as nib
import time
from .patch_ops import *
from .augmentations import *
from .rotate import rotate_vol_2d


def normalize(x, a=-1, b=1):
    orig_min = x.min()
    orig_max = x.max()

    numer = (x - orig_min) * (b - a)
    denom = orig_max - orig_min

    return a + numer / denom, orig_min, orig_max


class TrainSet(Dataset):
    def __init__(
        self,
        img_fpath,
        patch_size,
        n_patches,
        patch_axis,
        n_rots=6,
        augment=True,
        dtype=np.float32,
    ):
        self.n_patches = n_patches
        self.patch_size = patch_size
        self.patch_axis = patch_axis
        self.augment = augment

        if n_rots == 1:
            angles = [0]
        else:
            angles = range(0, 90 + 1, 90 // (n_rots - 1))

        img = nib.load(img_fpath).get_fdata(dtype=dtype)
        img, self.orig_min, self.orig_max = normalize(img)

        self.imgs_rot = [rotate_vol_2d(img, angle) for angle in angles]
        self.centers = get_random_centers(
            self.imgs_rot, self.patch_size, self.n_patches, weighted=True
        )

    def __len__(self):
        return self.n_patches

    def __getitem__(self, i):
        rot_i, center_idx = self.centers[i]
        center = [
            item[0]
            for item in np.unravel_index([center_idx], self.imgs_rot[rot_i].shape)
        ]
        patch = get_patch(
            self.imgs_rot[rot_i],
            center,
            self.patch_size + (1,),
        )

        if self.augment:
            patch = flip_xy(patch)

        return patch
