import numpy as np
from scipy.ndimage import gaussian_filter


def get_patch(img_rot, patch_center, patch_size):
    """
    img_rot: np.array, the HR in-plane image at a single rotation
    patch_center: tuple of ints, center position of the patch
    patch_size: tuple of ints, the patch size in 3D. For 2D patches, supply (X, Y, 1).
    mode: str, either "AA" or "SR".
                "AA": anti-aliasing, returns (patch_alias, patch_blur)
                "SR": super-res, returns (patch_alias, patch_hr)
    """

    # Get random rotation and center
    sts = [c - int(np.floor(p/2))
           for c, p in zip(patch_center, patch_size)]
    ens = [st + p for st, p in zip(sts, patch_size)]
    idx = tuple(slice(st, en) for st, en in zip(sts, ens))

    return img_rot[idx].squeeze()

def get_random_centers(imgs_rot, patch_size, n_patches, weighted=True):
    rot_choices = np.random.randint(0, len(imgs_rot), size=n_patches)
    centers = []
    for i, img_rot in enumerate(imgs_rot):
        n_choices = int(np.sum(rot_choices == i))
        if weighted:
            smooth = gaussian_filter(img_rot, 1.0)
            grads = np.gradient(smooth)
            grad_mag = np.sum([np.sqrt(np.abs(grad)) for grad in grads], axis=0)
            grad_mag[:int(np.floor(patch_size[0] / 2)), :, :] = 0.0
            grad_mag[:, :int(np.floor(patch_size[1] / 2)), :] = 0.0
            grad_mag[-int(np.ceil(patch_size[0] / 2)):, :, :] = 0.0
            grad_mag[:, -int(np.ceil(patch_size[1] / 2)):, :] = 0.0
            grad_probs = grad_mag.flatten()/grad_mag.sum()
            rand_idxs = np.random.choice(np.arange(0, np.prod(img_rot.shape)), size=n_choices, p=grad_probs)
        else:
            rand_idxs = np.random.choice(np.arange(0, np.prod(img_rot.shape)), size=n_choices)
        centers.extend((i, int(center)) for center in rand_idxs)
    np.random.shuffle(centers)
    return centers