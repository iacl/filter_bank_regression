import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader
import numpy as np
import sys
from resize.scipy import resize
from pathlib import Path
import nibabel as nib
from tqdm import tqdm
import argparse

from .models.filter_bank_networks import *
from .models.losses import *
from .utils.train_loader_1D_FB import normalize, inv_normalize
from .utils.train_loader_patches import TrainSet as TrainSetPatches
from .utils.filter_bank_utils import *

torch.backends.cudnn.benchmark = True


def main(args=None):
    #################### ARGUMENTS ####################
    parser = argparse.ArgumentParser()
    parser.add_argument("--inp-fpath", type=str, required=True)
    parser.add_argument("--out-fpath", type=str, required=True)
    parser.add_argument("--filter-dir", type=str, required=True)
    parser.add_argument("--fwhm", type=int, required=True)
    parser.add_argument("--gap", type=int, required=True)
    parser.add_argument("--acq-dim", type=int, choices=[2, 3], default=2)
    parser.add_argument("--gpu-id", type=int, default=0)
    parser.add_argument("--patch-size", type=int, default=32)
    parser.add_argument("--batch-size", type=int, default=32)
    parser.add_argument("--n-steps", type=int, default=10000)

    args = parser.parse_args(args if args is not None else sys.argv[1:])

    print("=" * 20, "STAGE 2", "=" * 20)

    inp_fpath = Path(args.inp_fpath)
    filter_dir = Path(args.filter_dir)
    out_fpath = Path(args.out_fpath)
    fwhm = args.fwhm
    gap = args.gap
    M = fwhm + gap
    device = torch.device(f"cuda:{args.gpu_id}")

    # Load filters to GPU
    hs_fpath = filter_dir / "hs.pt"
    fs_fpath = filter_dir / "fs.pt"

    hs = torch.load(hs_fpath)
    fs = torch.load(fs_fpath)

    hs = [h.to(device) for h in hs]
    fs = [f.to(device) for f in fs]

    print("*" * 10, "Training Phase", "*" * 10)
    # Data loader
    p = args.patch_size
    # restrict the patch size
    # max_patch_size = min(p * M, 128)
    # patch_size = (p, max_patch_size)
    # Above: in experimental work we tried dynamic patch sizes
    # In production we use fixed 32 x 32 patches
    patch_size = (p, p)
    d_patch_size = (p // 2, p // 2)
    batch_size = args.batch_size

    ds_stage_2 = TrainSetPatches(
        img_fpath=inp_fpath,
        patch_size=patch_size,
        n_rots=6,
        n_patches=int(args.n_steps * batch_size),
        patch_axis=2,
        augment=True,
    )

    loader_stage_2 = DataLoader(
        ds_stage_2,
        batch_size=batch_size,
        shuffle=True,
        pin_memory=True,
        num_workers=8,
    )

    # Network setup

    R = Network(
        inp_ch=1,
        out_ch=M - 1,
        M=M,
        L=16,
        features=128 * M,
        act="lrelu",
    ).to(device)

    D_d = Network(
        inp_ch=M - 1,
        out_ch=M - 1,
        M=M,
        L=2,
        features=64 * M,
        act="lrelu",
        out_patch_size=d_patch_size,
    ).to(device)

    lr = 1e-4
    opt_R = torch.optim.AdamW(R.parameters(), lr=lr)
    opt_D_d = torch.optim.AdamW(D_d.parameters(), lr=lr)

    signal_loss_fn = torch.nn.L1Loss()
    adv_loss_fn = torch.nn.BCEWithLogitsLoss()

    scheduler_R = torch.optim.lr_scheduler.OneCycleLR(
        optimizer=opt_R,
        max_lr=lr,
        total_steps=args.n_steps + 1,
        cycle_momentum=True,
    )

    scheduler_D_d = torch.optim.lr_scheduler.OneCycleLR(
        optimizer=opt_D_d,
        max_lr=lr,
        total_steps=args.n_steps + 1,
        cycle_momentum=True,
    )

    opt_R.step()
    opt_D_d.step()

    detail_lambda = 100
    adv_r_d_lambda = 1
    adv_d_d_lambda = 1

    d_d_freq = 1
    n_d_d_steps = 1

    scaler_R = torch.cuda.amp.GradScaler()
    scaler_D_d = torch.cuda.amp.GradScaler()

    # Training

    real_d_labels = torch.ones(
        (batch_size, M - 1, *d_patch_size), requires_grad=False
    ).to(device)
    fake_d_labels = torch.zeros(
        (batch_size, M - 1, *d_patch_size), requires_grad=False
    ).to(device)

    with tqdm(total=args.n_steps) as pbar:
        pbar_dict = {
            "R_loss": np.finfo(np.float32).max,
            "R_d": np.finfo(np.float32).max,
            "R_d_adv": np.finfo(np.float32).max,
            "D_d_adv": np.finfo(np.float32).max,
        }
        for cur_batch, xs_cpu in enumerate(loader_stage_2):
            N = xs_cpu.shape[0]
            xs = xs_cpu.to(device)

            opt_R.zero_grad()

            ########## TRAIN REGRESSOR ##########
            # freeze the discriminator
            for p in D_d.parameters():
                p.requires_grad = False

            with torch.cuda.amp.autocast():
                # rearrange as 1D
                xs_1d = batch_2d_to_1d(xs)
                # send through analysis bank
                cs, *ds = analyze(xs_1d, [h.detach() for h in hs])

                # rearrange as 2D
                cs_2d = batch_1d_to_2d(cs, patch_size[0]).unsqueeze(1)
                ds_2d = torch.stack(
                    [batch_1d_to_2d(d, patch_size[0]) for d in ds], dim=1
                )  # stack along channel dim

                # estimate details
                ds_hat_2d = R(cs_2d)

                ##### Losses #####

                # detail loss in 2D
                ds_loss = (
                    detail_lambda
                    * torch.stack(
                        [
                            signal_loss_fn(d_hat_2d, d_2d)
                            for d_hat_2d, d_2d in zip(ds_hat_2d, ds_2d)
                        ]
                    ).mean()
                )

                # adv loss on details
                r_d_adv_loss = adv_r_d_lambda * adv_loss_fn(
                    D_d(ds_hat_2d), real_d_labels[:N]
                )

                loss = 0
                loss += ds_loss
                loss += r_d_adv_loss

                scaler_R.scale(loss).backward()
                scaler_R.step(opt_R)
                scaler_R.update()

            pbar_dict["R_loss"] = loss.detach().cpu().numpy().item()
            pbar_dict["R_d"] = ds_loss.detach().cpu().numpy().item()
            pbar_dict["R_d_adv"] = r_d_adv_loss.detach().cpu().numpy().item()

            pbar.set_postfix(
                {
                    k: f"{v:.4f}"
                    for k, v in pbar_dict.items()
                    if v != 0 and v != np.finfo(np.float32).max
                }
            )

            ########## Train D_d ##########

            if cur_batch % d_d_freq == 0:
                # unfreeze the discriminator
                for p in D_d.parameters():
                    p.requires_grad = True
                for _ in range(n_d_d_steps):
                    opt_D_d.zero_grad()

                    with torch.cuda.amp.autocast():
                        # Measure discriminator's ability to classify real from generated samples
                        if np.random.rand() < 0.5:
                            d_d_loss = adv_d_d_lambda * adv_loss_fn(
                                D_d(ds_2d), real_d_labels[:N]
                            )
                        else:
                            d_d_loss = adv_d_d_lambda * adv_loss_fn(
                                D_d(ds_hat_2d.detach()), fake_d_labels[:N]
                            )

                        scaler_D_d.scale(d_d_loss).backward()
                        scaler_D_d.step(opt_D_d)
                        scaler_D_d.update()

                    pbar_dict["D_d_adv"] = d_d_loss.detach().cpu().numpy().item()

                    pbar.set_postfix(
                        {
                            k: f"{v:.4f}"
                            for k, v in pbar_dict.items()
                            if v != 0 and v != np.finfo(np.float32).max
                        }
                    )

            scheduler_D_d.step()
            scheduler_R.step()

            pbar.update(1)

    print("*" * 10, "Testing Phase", "*" * 10)
    # Run the model
    lr_obj = nib.load(inp_fpath)
    lr_vol = lr_obj.get_fdata(dtype=np.float32)
    # pad to square in-plane
    sq_p = np.abs(lr_vol.shape[0] - lr_vol.shape[1]) // 2
    idx_to_pad = np.argmin(lr_vol.shape[:2])
    pads = [(0, 0), (0, 0)]
    pads.insert(idx_to_pad, (sq_p, sq_p))
    pads = tuple(pads)
    lr_vol = np.pad(lr_vol, pads, mode="reflect")
    lr_vol, orig_min, orig_max = normalize(lr_vol)

    # axis 0 volume output
    hr_vol_0 = []
    print("Applying model axis 0...")
    for i in tqdm(range(lr_vol.shape[0])):
        x = lr_vol[i, :, :]

        p = int(1.5 * M)
        x = np.pad(x, ((0, 0), (p, p)), mode="reflect")

        x = torch.from_numpy(x).to(device)

        with torch.cuda.amp.autocast():
            ds_hat = R(x.unsqueeze(0).unsqueeze(1)).squeeze(0)

            x_hat = synthesize([x, *ds_hat], fs)
            # clip min to input intensity min
            x_hat[x_hat < x.min()] = x.min()
        #         x_hat[x_hat > x.max()] = x.max()

        x = x.detach().cpu().numpy().astype(np.float32)[:, p:-p]
        x_hat = x_hat.detach().cpu().numpy().astype(np.float32)[:, p * M : -p * M]
        hr_vol_0.append(x_hat)

    hr_vol_0 = np.array(hr_vol_0)[sq_p:-sq_p]
    hr_vol_0 = inv_normalize(hr_vol_0, orig_min, orig_max, a=-1, b=1)

    # axis 1 volume output
    hr_vol_1 = []
    print("Applying model axis 1...")
    for i in tqdm(range(lr_vol.shape[1])):
        x = lr_vol[:, i, :]

        p = int(1.5 * M)
        x = np.pad(x, ((0, 0), (p, p)), mode="reflect")

        x = torch.from_numpy(x).to(device)

        with torch.cuda.amp.autocast():
            ds_hat = R(x.unsqueeze(0).unsqueeze(1)).squeeze(0)

            x_hat = synthesize([x, *ds_hat], fs)
            # clip min to input intensity min
            x_hat[x_hat < x.min()] = x.min()
        #         x_hat[x_hat > x.max()] = x.max()

        x = x.detach().cpu().numpy().astype(np.float32)[:, p:-p]
        x_hat = x_hat.detach().cpu().numpy().astype(np.float32)[:, p * M : -p * M]
        hr_vol_1.append(x_hat)

    hr_vol_1 = np.array(hr_vol_1).transpose(1, 0, 2)[sq_p:-sq_p]
    hr_vol_1 = inv_normalize(hr_vol_1, orig_min, orig_max, a=-1, b=1)

    mean_vol_out = np.mean([hr_vol_0, hr_vol_1], axis=0)

    # Update header
    affine = lr_obj.affine
    header = lr_obj.header

    new_scales = [1, 1]
    new_scales.insert(2, 1 / M)
    new_scales = tuple(new_scales)
    new_affine = np.matmul(affine, np.diag(new_scales + (1,)))
    # Write nifti
    out_obj = nib.Nifti1Image(
        mean_vol_out,
        affine=new_affine,
        header=header,
    )
    nib.save(out_obj, out_fpath)
    print(f"Result written to {out_fpath}")
